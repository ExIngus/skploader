/**
 * вывод лога включается переменной LOGGER
 * Вывод лога через OutputDebugString
 * 
 */
#ifndef LOG_H
#define LOG_H

#ifdef LOGGER
#include <stdio.h>
#include <windows.h>


	void loggerPrintfDebugStringW(LPCWSTR msg, ...);
	void loggerPrintfDebugStringA(LPCSTR msg, ...);

//#pragma message "Logger enabled. See debug log. (Dbgview)"

#define log(msg) OutputDebugStringW(msg)
#define logArg(msg,args...) loggerPrintfDebugStringW(msg,args)
#define logArgA(msg,args...) loggerPrintfDebugStringA(msg,args)
#define whenLog #ifdef LOGGER
#else
#define log(msg)
#define logArg(msg,args...)
#define logArgA(msg,args...)
#endif

#endif /* LOG_H */

