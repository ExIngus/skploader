#ifndef ASPELLCONTROLLER_H
#define ASPELLCONTROLLER_H
#include "lib/hunspell/hunspell.h"

class SpellChecker {
public:
	SpellChecker(const char * affPath,const char * dictPath);
	virtual ~SpellChecker();
	bool spell(const char * word);
	/**
	 *
	 * @param word
	 * @param suggestions
	 * @return 0 = good word  or suggestions count
	 */
	int spellEx(const char * word, char** suggestions);
private:
	Hunspell *hunspell;
};

#endif /* ASPELLCONTROLLER_H */

