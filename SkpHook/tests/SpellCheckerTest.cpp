
#include <stdlib.h>
#include <iostream>
#include "lib/hunspell/hunspell.h"

const char *dictAffPath = "../SkpLoader/dict/Ru-En.aff"; //path to the .aff file
const char *dictDicPath = "../SkpLoader/dict/Ru-En.dic"; //path to the .dic file

void test1() {
	std::cout << "SpellCheckerTest test 1" << std::endl;

	Hunhandle *handle = Hunspell_create(dictAffPath, dictDicPath);
	if (!handle) {
		std::cout << "%TEST_FAILED% time=0 testname=test1 (SpellCheckerTest) message=invalid handle" << std::endl;
	}
	char* encode = Hunspell_get_dic_encoding(handle);
	std::cout << "encode=" << encode << std::endl;

	//	Hunspell_

	Hunspell_add(handle, "word");
	if (Hunspell_spell(handle, "привет") != 0) {
		//not 0 = good word
		std::cout << "good word" << std::endl;
	} else {
		//0 = bad word
		std::cout << "bad word" << std::endl;
		//		std::cout << "%TEST_FAILED% time=0 testname=test1 (SpellCheckerTest) message=invalid word" << std::endl;
	}

	char** suggestList;
	int count = Hunspell_suggest(handle, &suggestList, "првет");

	std::cout << "suggestList" << std::endl;
	for (int i = 0; i < count; i++) {
		char* w = suggestList[i];
		std::cout << w << std::endl;
	}
	Hunspell_free_list(handle, &suggestList, count);

	Hunspell_destroy(handle);
}


int main(int argc, char** argv) {
	std::cout << "%SUITE_STARTING% SpellCheckerTest" << std::endl;
	std::cout << "%SUITE_STARTED%" << std::endl;

	std::cout << "%TEST_STARTED% test1 (SpellCheckerTest)" << std::endl;
	test1();
	std::cout << "%TEST_FINISHED% time=0 test1 (SpellCheckerTest)" << std::endl;
	//
	//	std::cout << "%TEST_STARTED% test2 (SpellCheckerTest)" << std::endl;
	//	test2();
	//	std::cout << "%TEST_FINISHED% time=0 test2 (SpellCheckerTest)" << std::endl;

	//	std::cout << "%TEST_STARTED% test2 (SpellCheckerTest)\n" << std::endl;
	//	test2();
	//	std::cout << "%TEST_FINISHED% time=0 test2 (SpellCheckerTest)" << std::endl;

	std::cout << "%SUITE_FINISHED% time=0" << std::endl;

	return (EXIT_SUCCESS);
}

