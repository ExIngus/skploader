/**
 * чтобы убрать @ в мени функций в gcc -Wl,--subsystem,windows,--kill-at
 */
#define ADDCALL __cdecl

#ifndef SKPHOOK_H
#define SKPHOOK_H

#include <windows.h>
#include "Log.h"


__declspec(dllexport)
#define EXPORT_DLL __declspec(dllexport)

EXPORT_DLL BOOL APIENTRY DllMain(PVOID hModule, ULONG ulReason, PCONTEXT pctx);
EXPORT_DLL LRESULT CALLBACK OnCallWndProc(INT nCode, WPARAM wParam, LPARAM lParam);
EXPORT_DLL LRESULT CALLBACK OnMouseProc(INT nCode, WPARAM wParam, LPARAM lParam);

#define MAX_STACK_STR 256
#define MAX_CLASS_NAME 256

//Классы окон
#define WNC_MAIN_FORM L"tSkMainForm"
#define WNC_BANNER L"TChatBanner"
#define WNC_CONTACT_LIST L"TConversationsControl"
#define WNC_CHAT_FORM L"TConversationForm"
#define WNC_CHAT_RICH_EDIT L"TChatRichEdit"
#define WNC_CHAT_ENTRY_CONTROL L"TChatEntryControl"
#define WNC_CONVERSATION_HEADER L"TConversationHeader"
#define WNC_SPLITTER  L"TSkypeSplitter"
#define WNC_DIVIDER  L"TDivider"
#define WNC_CHAT_CONTENT_CONTROL  L"TChatContentControl"

//Библиотека
#define HLIB_NAME L"libSkpHook.dll"
#define HLIB_CALL_WHN_PROC "OnCallWndProc"
#define HLIB_MOUSE_PROC "OnMouseProc"
#define EXE_NAME L"Skype.exe"

#endif /* SKPHOOK_H */
