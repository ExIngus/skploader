/**
 * вывод лога включается переменной LOGGER
 * Вывод лога через OutputDebugString
 *
 */
#include "Log.h"
#ifdef LOGGER
#define MAX_DEBUG_STRING 1024

void loggerPrintfDebugStringW(LPCWSTR msg, ...) {
	va_list args;
	va_start(args, msg);
	WCHAR buff[MAX_DEBUG_STRING];
	vswprintf(buff, MAX_DEBUG_STRING, msg, args);
	OutputDebugStringW(buff);
	va_end(args);
}

void loggerPrintfDebugStringA(LPCSTR msg, ...) {
	va_list args;
	va_start(args, msg);
	CHAR buff[MAX_DEBUG_STRING];
	vsprintf(buff, msg, args);
	OutputDebugStringA(buff);
	va_end(args);
}
#endif