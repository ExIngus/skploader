#include "SkpHook.h"
#include <windows.h>
#include "lib/hunspell/hunspell.h"
#include "lib/hunspell/hunspell.h"
#include <richedit.h>
#include <wchar.h>
#include <stdio.h>

#define HIDE_CHAT_BANNER //прятать баннер

#define MARK_WORD_COLOR RGB(255, 0, 0)
#define NORMAL_WORD_COLOR RGB(0, 0, 0)
#define MAX_CHAR_IN_RICH_EDIT 8000
#define RE_UPDATE_TEXT 0xbd01
#define MARGIN_FORM_RIGHT 15
#define MARGIN_FORM_BOTTOM 60

/*
 * Пояснения по кодировке:
 * WinApi W функции работают со строками LPWSTR(wchar_t) в кодировке UTF16(2 байта на символ)
 * Словарь хранится в мультибайтовой UTF8(от 1 до 6 байт на символ)
 */
#define WCS2MBS(sourceStr,targetStrVarName) CHAR targetStrVarName[wstr2mbstr((sourceStr),0,0)];\
	wstr2mbstr((sourceStr), targetStrVarName, sizeof(targetStrVarName));
#define MBS2WCS(sourceStr,targetStrVarName) WCHAR targetStrVarName[mbstr2wstr((sourceStr),0,0)];\
	mbstr2wstr((sourceStr), targetStrVarName, sizeof(targetStrVarName));
#define MAX_WORD_LEN 60 //максимальна длинна слова

const char *affPath = "dict\\Ru-En.aff"; //path to the .aff file
const char *dictPath = "dict\\Ru-En.dic"; //path to the .dic file
const char *userDictPath = "dict\\userDict.txt"; //path to userDict

Hunhandle *hunHandle;
HWND hwndMain;
FILE *fUserDict;

/**
 * Преобразует строку UTF8 в UTF16
 * при trgText==0 и size=0 вернёт необходимый размер буфера в байтах
 * @param text
 * @param trgText
 * @param size
 * @return size количество записанных в trgText символов
 */
int wstr2mbstr(LPWSTR text, LPTSTR trgText, int sizeBytes) {
	int bytes = WideCharToMultiByte(CP_UTF8, 0, text, -1, trgText, sizeBytes, 0, 0);
	return bytes;
}

/**
 * Преобразует строку UTF16 в UTF8
 * при trgText==0 и size=0 вернёт необходимый размер буфера в байтах
 * @param text
 * @param trgText
 * @param size
 * @return size количество записанных в trgText символов
 */
int mbstr2wstr(LPTSTR text, LPWSTR trgText, int size) {
	//char->wchar_t
	int sizeBytes = size * sizeof (WCHAR);
	int bytes = MultiByteToWideChar(CP_UTF8, 0, text, -1, trgText, sizeBytes);
	return bytes / sizeof (WCHAR);
}

/**
 * Находит и скрывает окно TChatBanner
 * @param hwndParent
 * @return TRUE если окно найдено
 */
boolean fixChatBanner(HWND hwndParent) {
	HWND hwBanner = FindWindowExW(hwndParent, 0, WNC_BANNER, 0);
	if (hwBanner) {
		SetWindowPos(hwBanner, HWND_BOTTOM, 0, 0, 0, 0, SWP_HIDEWINDOW);
		log(L"fix ChatBanner");
		return TRUE;
	}
	return FALSE;
}

void hideBannerOneWindow(HWND hwForm) {
	fixChatBanner(hwForm);
	//Окно списка контактов
	HWND hwList = FindWindowExW(hwndMain, 0, WNC_CONTACT_LIST, 0);
	RECT rectMain;
	GetWindowRect(hwndMain, &rectMain);
	RECT rectList;
	GetWindowRect(hwList, &rectList);
	//Рассчитываем размер окна чата: ширина главного окна минус ширина списка контактов и припуск; высота минус припуск
	LONG mainWidth = rectMain.right - rectMain.left;
	LONG mainHeight = rectMain.bottom - rectMain.top;
	LONG listWidth = rectList.right - rectList.left;
	LONG listHeight = rectList.bottom - rectList.top;

	LONG newX = listWidth;
	LONG newY = 0;
	LONG newWidth = mainWidth - listWidth - MARGIN_FORM_RIGHT;
	LONG newHeight = mainHeight - MARGIN_FORM_BOTTOM;

	SetWindowPos(hwForm, 0, newX, newY, newWidth, newHeight, 0);
	logArg(L"hideBannerOneWindow(%p)\n"
			" main.x=%ld main.y=%ld main.width=%ld main.height=%ld\n"
			" list.x=%ld list.y=%ld list.width=%ld list.height=%ld\n"
			" new form.x=%ld form.y=%ld form.width=%ld form.height=%ld",
			hwForm
			, rectMain.left, rectMain.top, mainWidth, mainHeight
			, rectList.left, rectList.top, listWidth, listHeight
			, newX, newY, newWidth, newHeight
			);
}

//BOOL hideBannerMultiWindow(HWND hwWin, LPWSTR className) {
//	if (lstrcmpW(className, WNC_CONVERSATION_HEADER) == 0
//			|| lstrcmpW(className, WNC_DIVIDER) == 0
//			|| lstrcmpW(className, WNC_CHAT_CONTENT_CONTROL) == 0) {
//		return FALSE;
//	}
//
//	if (lstrcmpW(className, WNC_CHAT_FORM) == 0) {
//		HWND hwForm = hwWin;
//		fixChatBanner(hwForm);
//		//TConversationHeader
//		//TDivider
//		//TChatContentControl
//		HWND hwHeader = FindWindowExW(hwForm, 0, WNC_CONVERSATION_HEADER, 0);
//		HWND hwDivider = FindWindowExW(hwForm, 0, WNC_DIVIDER, 0);
//		HWND hwChat = FindWindowExW(hwForm, 0, WNC_CHAT_CONTENT_CONTROL, 0);
//
//		if (!hwHeader || !hwDivider || !hwChat) {
//			logArg(L"hideBannerMultiWindow: Not found windows: %p %p %p", hwHeader, hwDivider, hwChat);
//			return TRUE;
//		}
//
//		RECT rectForm;
//		GetWindowRect(hwForm, &rectForm);
//		LONG formWidth = rectForm.right - rectForm.left;
//		//	LONG formHeight = rectForm.bottom - rectForm.top;
//
//		RECT rectHeader;
//		GetWindowRect(hwHeader, &rectHeader);
//		LONG headerHeight = rectHeader.bottom - rectHeader.top;
//		SetWindowPos(hwHeader, 0, 0, 0, formWidth, headerHeight, 0);
//
//		RECT rectDivider;
//		GetWindowRect(hwDivider, &rectDivider);
//		LONG dividerHeight = rectDivider.bottom - rectDivider.top;
//		SetWindowPos(hwDivider, 0, 0, headerHeight, formWidth, dividerHeight, 0);
//
//		RECT rectChat;
//		GetWindowRect(hwChat, &rectChat);
//		LONG chatY = headerHeight + dividerHeight;
//		LONG chatHeight = rectChat.bottom - chatY;
//		SetWindowPos(hwChat, 0, 0, chatY, formWidth, chatHeight, 0);
//		MoveWindow(hwChat, 0, chatY, formWidth, chatHeight, FALSE);
//
//		GetWindowRect(hwChat, &rectChat);
//		logArg(L"hideBannerMultiWindow(%p) header(%p) divider(%p) chat(%p)  \n"
//				" new chat.x=%ld chat.y=%ld chat.width=%ld chat.height=%ld",
//				hwForm, hwHeader, hwDivider, hwChat
//				//			, 0, chatY,formWidth,chatHeight
//				, rectChat.left, rectChat.top, rectChat.right - rectChat.left, rectChat.bottom - rectChat.top
//				);
//		return FALSE;
//	}
//	return TRUE;
//}

/**
 * Пробует скрыть окно TChatBanner(см fixChatBanner) 
 * и корректирует размер окна чата TConversationsControl
 * @param hwForm
 */
BOOL hideBanner(HWND hwForm, LPWSTR className) {
#ifdef HIDE_CHAT_BANNER

	static volatile BOOL isOneWindow;
	if (lstrcmpW(className, WNC_CHAT_FORM) == 0) {
		isOneWindow = IsChild(hwndMain, hwForm);
	}

	if (isOneWindow) {
		if (lstrcmpW(className, WNC_CHAT_FORM) == 0) {
			hideBannerOneWindow(hwForm);
			return FALSE;
		}
	} else {
		//TODO: написать рабочую версию, нужно менять размер каждого из окон при поступлении WM_SIZE для этого окна
//		return hideBannerMultiWindow(hwForm, className);
	}

#endif
	return TRUE;
}

/**
 * Убирает выделения со всего текста
 * @param hwndRichEdit
 */
void tokenClean(HWND hwndRichEdit) {
	log(L"tokenClean");
	CHARFORMATW cf;
	memset(&cf, 0, sizeof (cf));
	cf.cbSize = sizeof (cf);
	cf.dwMask = CFM_COLOR;
	cf.crTextColor = NORMAL_WORD_COLOR;
	SendMessageW(hwndRichEdit, EM_SETCHARFORMAT, SCF_ALL, (LPARAM) & cf);
}

/**
 * Выдиляет часть текста цветом
 * @param hwndRichEdit
 * @param cMin
 * @param cMax
 * @param color
 */
void tokenSet(HWND hwndRichEdit, LONG cMin, LONG cMax, COLORREF color) {
	logArg(L"tokenSet min=%ld max=%ld", cMin, cMax);
	CHARRANGE chrgSave;
	CHARRANGE chrg;
	memset(&chrg, 0, sizeof (chrg));
	chrg.cpMin = cMin;
	chrg.cpMax = cMax;
	SendMessageW(hwndRichEdit, EM_EXGETSEL, 0, (LPARAM) & chrgSave);
	SendMessageW(hwndRichEdit, EM_EXSETSEL, 0, (LPARAM) & chrg);
	CHARFORMATW cf;
	memset(&cf, 0, sizeof (cf));
	cf.cbSize = sizeof (cf);
	cf.dwMask = CFM_COLOR;
	cf.crTextColor = color;
	SendMessageW(hwndRichEdit, EM_SETCHARFORMAT, SCF_SELECTION | SCF_WORD, (LPARAM) & cf);
	SendMessageW(hwndRichEdit, EM_EXSETSEL, 0, (LPARAM) & chrgSave);
}

/**
 * Отключает перерисовку окна RichEdit
 * @param hwndRichEdit
 * @return old eventMask
 */
int disableRedraw(HWND hwndRichEdit) {
	int eventMask = SendMessageW(hwndRichEdit, EM_SETEVENTMASK, 0, 0);
	SendMessageW(hwndRichEdit, WM_SETREDRAW, FALSE, 0);
	return eventMask;
}

/**
 * Включает перерисовку окна RichEdit
 * @param hwndRichEdit
 * @return
 */
void enableRedraw(HWND hwndRichEdit, int eventMask) {
	SendMessageW(hwndRichEdit, WM_SETREDRAW, TRUE, 0);
	SendMessageW(hwndRichEdit, EM_SETEVENTMASK, 0, eventMask);
}

/**
 * Проверяет является ли символ разделителем слов
 * @param ch
 * @return
 */
boolean isDelimetr(WCHAR ch) {
	return (ch >= 0x0000 && ch <= 0x0040) //управляющие символы, знаки пунктуации и цифры
			|| (ch >= 0x005b && ch <= 0x0060) //знаки пунктуации и цифры
			|| (ch >= 0x007b && ch <= 0x00bf) //знаки пунктуации и цифры
			//				|| ch == '\n' || ch == '\r'
			;
}

/**
 * Изменился текст в RichEdit
 * @param hwnd
 * @param text
 */
void OnRichEditText(HWND hwnd, LPWSTR text) {
	logArg(L"RichEdit text %s", text);
	tokenClean(hwnd);
	int eventMask = disableRedraw(hwnd);

	int textLn = wcslen(text);
	int lastPos = textLn - 1;
	boolean inWord = FALSE; // i внутри слова
	int wordBegin = 0;
	int countRN = 0; //счётчик '\r' для коррекции tokenSet, т.е. RichEdit считает "\r\n" за один символ
	for (int i = 0; i < textLn; i++) {
		WCHAR ch = text[i];
		boolean lastChar = FALSE;
		if (ch == '\r') {
			countRN++;
		}
		if (isDelimetr(ch) || (lastChar = (i == lastPos))) {
			//разделитель или последний символ строки(lastChar))
			if (!inWord) {
				continue;
			} else {
				//слово кончилось
				inWord = FALSE;
				if (!lastChar) {//последний символ строки и не разделитель
					text[i] = 0;
				}
				//адрес начала слова
				LPWSTR wordW = (text + wordBegin);
				if (wcslen(wordW) <= MAX_WORD_LEN) {
					//преобразуем слово в мультибайтовую кодировку, понятную hunspell
					WCS2MBS(wordW, wordMB);
					boolean valid = Hunspell_spell(hunHandle, wordMB) != 0; //вердикт hunspell
					logArg(L"Word: pos=%d ln=%d valid=%i <%s>", wordBegin, (i - wordBegin), valid, wordW);
					if (!valid) {
						tokenSet(hwnd, wordBegin - countRN, (i + 1) - countRN, MARK_WORD_COLOR);
					}
				}
			}
		} else if (!inWord) {
			wordBegin = i;
			inWord = TRUE;
		}
	}
	//разрешаем перерисовку.
	enableRedraw(hwnd, eventMask);
}

/**
 * Загружает в Hunspell словарь пользователя,
 * инициализирует fUserDict
 */
void loadUserDict() {
	FILE *fDict = fopen(userDictPath, "r");
	if (fDict) {
		//словарь существует, загружаем в Hunspell
		char wordBuff[256];
		while (fscanf(fDict, "%s255", wordBuff) != EOF) {
			Hunspell_add(hunHandle, wordBuff);
		}
	}
	fclose(fDict);
	fUserDict = fopen(userDictPath, "a");
}

/**
 * Добавляет в Hunspell и словарь пользователя слово
 * @param wordW
 */
void addWordUserDict(LPWSTR wordW) {
	if (wcslen(wordW) <= MAX_WORD_LEN) {
		CharLowerW(wordW);
		WCS2MBS(wordW, wordMB)
		Hunspell_add(hunHandle, wordMB);
		fprintf(fUserDict, "%s\n", wordMB);
		fflush(fUserDict);
		logArg(L"addWordUserDict: word=<%s>", wordW);
	} else {
		logArg(L"addWordUserDict: error, long word=<%s>", wordW);
	}
}

/**
 * Обработка оконного события
 * @param hwnd
 * @param uiMessage
 * @param wParam
 * @param lParam
 * @return
 */
BOOL WINAPI HookProc(HWND hwnd, UINT uiMessage, WPARAM wParam, LPARAM lParam) {
	if (uiMessage == WM_SIZE) { //Изменение размеров окна
		WCHAR className[MAX_CLASS_NAME];
		GetClassNameW(hwnd, className, MAX_CLASS_NAME);
		return hideBanner(hwnd,className);
	} else if (uiMessage == RE_UPDATE_TEXT) {//Изменение текста в окне
		WCHAR className[MAX_CLASS_NAME];
		GetClassNameW(hwnd, className, 30);
		if (lstrcmpW(className, WNC_CHAT_RICH_EDIT) == 0) {
			int textLng = GetWindowTextLengthW(hwnd);
			if (textLng > 0 && textLng <= MAX_CHAR_IN_RICH_EDIT) {
				//Т.к. хук ставится для одного потока, можно,для ускорения, использовать статический буфер, вместо alloc
				static WCHAR textBuffer[MAX_CHAR_IN_RICH_EDIT + 1];
				//LPWSTR text = (LPWSTR) calloc(++textLng, sizeof (WCHAR)); //++textLng под 00
				GetWindowTextW(hwnd, textBuffer, MAX_CHAR_IN_RICH_EDIT);
				OnRichEditText(hwnd, textBuffer);
				//free(text);
			}
		}
	}
	return TRUE;
}

/**
 * Загрузка библиотеки в процесс
 * @param hModule
 * @param ulReason
 * @param pctx
 * @return
 */
BOOL APIENTRY DllMain(PVOID hModule, ULONG ulReason, PCONTEXT pctx) {
	UNREFERENCED_PARAMETER(hModule);
	UNREFERENCED_PARAMETER(pctx);

	if (ulReason == DLL_PROCESS_ATTACH) {// загрузка в процесс
		WCHAR patch[MAX_PATH];
		GetModuleFileNameW(0, patch, MAX_PATH);
		logArg(L"ATTACH PROCESS process:%s", patch);
		if (wcsstr(patch, L"Skype.exe")) {
			//далее инициализация только для Skype.exe
			hwndMain = FindWindowW(WNC_MAIN_FORM, 0);
			fixChatBanner(hwndMain);
			hunHandle = Hunspell_create(affPath, dictPath);
			if (hunHandle) {
				char* encode = Hunspell_get_dic_encoding(hunHandle);
				if (strcmpi(encode, "UTF-8") != 0) {
					//для мультиязычных словарей поддерживается только UTF-8
					OutputDebugStringW(L"Supported only dictionaries encoded UTF-8.");
				}
				logArgA("Init Hunspell. encode:<%s> aaf:<%s> dict:<%s> userDict:<%s>", encode, affPath, dictPath, userDictPath);
				//загрузка словоря пользователя
				loadUserDict();
			} else {
				log(L"Hunspell init error");
				return FALSE;
			}

		}
	} else if (ulReason == DLL_PROCESS_DETACH) {
		log(L"DETACH PROCESS");
	}

	return TRUE;
}

/**
 * Хук на оконную функцию WH_CALLWNDPROC
 * @param nCode
 * @param wParam
 * @param lParam
 * @return
 */
LRESULT CALLBACK OnCallWndProc(INT nCode, WPARAM wParam, LPARAM lParam) {
	PCWPSTRUCT pcwps = (PCWPSTRUCT) lParam;
	if ((nCode == HC_ACTION) && (pcwps) && (pcwps->hwnd) && (wParam == 0)) {
		return HookProc(pcwps->hwnd, pcwps->message, pcwps->wParam, pcwps->lParam);
	}
	if (nCode < 0) {
		return CallNextHookEx(0, nCode, wParam, lParam);
	}
	return 0;

}

boolean getWord(HWND hwndRichEdit, POINT point, LPWSTR wordBuff, int buffSize, CHARRANGE *wordRange) {
	//получаем координаты окна
	RECT rect;
	GetWindowRect(hwndRichEdit, &rect);
	//вычисляем координаты относительно окна, т.к. point это координаты относительно экрана
	POINTL pos = {point.x - rect.left, point.y - rect.top};
	long index = SendMessageW(hwndRichEdit, EM_CHARFROMPOS, 0, (WPARAM) & pos); // позиция под курсором
	long begin = SendMessageW(hwndRichEdit, EM_FINDWORDBREAK, WB_MOVEWORDLEFT, index); // начало слова
	long end = SendMessageW(hwndRichEdit, EM_FINDWORDBREAK, WB_RIGHTBREAK, index); // конец слова
	logArg(L"getWord poin(%d,%d) pos(%d,%d) word(%d(%d-%d))", point.x, point.y, pos.x, pos.y, index, begin, end);
	//	logArg(L"getWord %d %d %d", index, begin, end);
	long wordLng = end - begin;
	if (wordLng > 0 && wordLng < buffSize) {// проверяем что слово не слишком длинное
		wordRange->cpMin = begin;
		wordRange->cpMax = end;
		TEXTRANGEW range;
		range.lpstrText = wordBuff;
		range.chrg.cpMin = begin;
		range.chrg.cpMax = end;
		SendMessageW(hwndRichEdit, EM_GETTEXTRANGE, 0, (LPARAM) & range);
		// обрезаем полученный текст по первому разделителю, если он есть
		//некоторые разделители EM_FINDWORDBREAK не признаёт
		int i;
		for (i = 0; i < wordLng; i++) {
			if (isDelimetr(wordBuff[i])) {
				wordBuff[i] = 0;
				break;
			}
		}
		if (i == 0) {
			//если первый же символ - разделитель, и обрезали по нему
			return FALSE;
		}
		return TRUE;
	} else {
		return FALSE;
	}

}

void replaceWord(HWND hwndRichEdit, CHARRANGE *charRange, LPWSTR str) {
	int mask = disableRedraw(hwndRichEdit);
	SendMessageW(hwndRichEdit, EM_EXSETSEL, 0, (LPARAM) charRange);
	SendMessageW(hwndRichEdit, EM_REPLACESEL, TRUE, (LPARAM) str);
	enableRedraw(hwndRichEdit, mask);
	SendMessageW(hwndRichEdit, RE_UPDATE_TEXT, 0, 0);
}

/**
 * Обработка события мыши
 * @param hwnd
 * @param uiMessage
 * @param point
 * @param dwExtraInfo
 * @param wHitTestCode
 */
BOOL WINAPI MouseProc(HWND hwnd, WPARAM uiMessage, POINT point, ULONG_PTR dwExtraInfo, UINT wHitTestCode) {
	BOOL done = FALSE;
	volatile static BOOL isOpenMenu = FALSE; // переменная нужна для подавления повторного сообщения WM_RBUTTONUP при вызове TrackPopupMenuEx
	//	if (uiMessage != WM_MOUSEMOVE) {
	//		logArg(L"WM_ hwnd<%0X> uiMessage<%0X> wHitTestCode<%0X> dwExtraInfo<%0X> point<%d,%d>", hwnd,uiMessage,wHitTestCode,dwExtraInfo,point.x,point.y);
	//	}
	if (uiMessage == WM_RBUTTONUP && !isOpenMenu) {
		logArg(L"WM_RBUTTONUP hwnd<%0X> uiMessage<%0X> wHitTestCode<%0X> dwExtraInfo<%0X> point<%d,%d>", hwnd, uiMessage, wHitTestCode, dwExtraInfo, point.x, point.y);
		WCHAR className[MAX_CLASS_NAME];
		GetClassNameW(hwnd, className, MAX_CLASS_NAME);
		if (lstrcmpW(className, WNC_CHAT_RICH_EDIT) == 0) { //класс окна TChatRichEdit
			CHARRANGE charRange;
			SendMessageW(hwnd, EM_EXGETSEL, 0, (LPARAM) & charRange);
			//если есть выделенный текст, вероятно пользователь хочет увидеть контекстное меню для копирования, а не варианты написания слова
			if ((charRange.cpMax - charRange.cpMin) == 0) {
				WCHAR wordW[MAX_WORD_LEN + 1];
				if (getWord(hwnd, point, wordW, sizeof (wordW), &charRange)) {
					logArg(L"RichEdit selected text <%s>", wordW);
					//конвертируем слово в multiByte
					WCS2MBS(wordW, wordMB)

					boolean valid = Hunspell_spell(hunHandle, wordMB) != 0; //вердикт hunspell
					if (!valid) {// слово с ошибкой
						char** suggestList;
						int count = Hunspell_suggest(hunHandle, &suggestList, wordMB); //получаем список вариантов правильного слова
						//Показываем меню с вариантами слова
						HMENU hsubMenu = CreatePopupMenu();
						const int MENU_CANCEL = 0;
						const int MENU_ADD = 1;
						const int MENU_FIRST_WORD = 2;
						InsertMenuW(hsubMenu, 0, MF_BYPOSITION | MF_STRING, MENU_CANCEL, L"&Отмена");
						InsertMenuW(hsubMenu, -1, MF_BYPOSITION | MF_STRING, MENU_ADD, L"&Добавить");
						for (int i = 0; i < count; i++) {
							char* wordMB = suggestList[i];
							MBS2WCS(wordMB, wordW)
							logArg(L"suggest word %s", wordW);
							InsertMenuW(hsubMenu, -1, MF_BYPOSITION | MF_STRING, MENU_FIRST_WORD + i, wordW);
						}
						//показываем меню под курсором
						POINT p;
						GetCursorPos(&p);
						SetForegroundWindow(hwnd);
						isOpenMenu = TRUE;
						int id = (int) TrackPopupMenuEx(hsubMenu, TPM_TOPALIGN | TPM_LEFTALIGN | TPM_NONOTIFY | TPM_RETURNCMD | TPM_RIGHTBUTTON,
								p.x, p.y, hwnd, 0);
						logArg(L"TrackPopupMenu result %d", id);
						DestroyMenu(hsubMenu);
						isOpenMenu = FALSE;
						//обрабатываем выбор пользователя
						if (id == MENU_ADD) {//добавить новое слово в словарь
							addWordUserDict(wordW);
							SendMessageW(hwnd, RE_UPDATE_TEXT, 0, 0);
						} else if (id >= MENU_FIRST_WORD) { //заменить слово, выбранным словом
							id -= MENU_FIRST_WORD;
							MBS2WCS(suggestList[id], wordW)
							logArg(L"selected word %s", wordW);
							replaceWord(hwnd, &charRange, wordW);
						}
						done = TRUE;
						Hunspell_free_list(hunHandle, &suggestList, count);
					}
				}
			}
		}
	}
	return done;
}

/**
 * Хук на WH_MOUSE
 * @param nCode
 * @param wParam
 * @param lParam
 * @return
 */
LRESULT CALLBACK OnMouseProc(INT nCode, WPARAM wParam, LPARAM lParam) {
	PMOUSEHOOKSTRUCT mouseStr = (PMOUSEHOOKSTRUCT) lParam;
	if ((nCode == HC_ACTION)&&(mouseStr) && (mouseStr->hwnd)) {
		if (MouseProc(mouseStr->hwnd, wParam, mouseStr->pt, mouseStr->dwExtraInfo, mouseStr->wHitTestCode)) {
			//сообщение обработано, и не будет передано дальше
			log(L"MouseProc DONE");
			return 1;
		}
	}
	return CallNextHookEx(0, nCode, wParam, lParam);
}
