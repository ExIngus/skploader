#include "../SkpHook/SkpHook.h"
#include "../SkpHook/Log.h"
#include <windows.h>
#include <wchar.h>

void printErr(LPCWSTR mark) {
	//https://msdn.microsoft.com/en-us/library/windows/desktop/ms681381%28v=vs.85%29.aspx
	DWORD err = GetLastError();
	LPWSTR pBuffer;
	FormatMessageW(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM, NULL, err,
			MAKELANGID(LANG_ENGLISH, SUBLANG_DEFAULT), (LPWSTR) & pBuffer, 0, NULL);
	WCHAR strBuff[MAX_STACK_STR];
	wsprintfW(strBuff, L"Error:%s: %ld : %s", mark, err, pBuffer);
	OutputDebugStringW(strBuff);
}

HHOOK hCallWndHook;
HHOOK hMouseHook;

boolean InstallHook(DWORD dwThreadID) {
	if (dwThreadID) {
		HMODULE mHook = LoadLibraryW(HLIB_NAME);
		if (mHook) {
			FARPROC pCallWndProc = GetProcAddress(mHook, HLIB_CALL_WHN_PROC);
			FARPROC pMouseProc = GetProcAddress(mHook, HLIB_MOUSE_PROC);
			if (pCallWndProc && pMouseProc) {
				hCallWndHook = SetWindowsHookExW(WH_CALLWNDPROC, (HOOKPROC) pCallWndProc, mHook, dwThreadID);
				hMouseHook = SetWindowsHookExW(WH_MOUSE, (HOOKPROC) pMouseProc, mHook, dwThreadID);
				if (hCallWndHook && hMouseHook) {
					return TRUE;
				} else {
					printErr(L"InstallHook:SetWindowsHookExW");
					return FALSE;
				}
			} else {
				printErr(L"InstallHook:GetProcAddress");
				return FALSE;
			}
		} else {
			printErr(L"InstallHook:LoadLibraryW");
			return FALSE;
		}

	} else {
		printErr(L"InstallHook:invalid_dwThreadID");
		return FALSE;
	}
}

boolean InstallWindowHook(HWND hWnd) {
	if ((hWnd) && (IsWindow(hWnd))) {
		DWORD dwThreadID = GetWindowThreadProcessId(hWnd, NULL);
		return InstallHook(dwThreadID);
	} else {
		printErr(L"InstallWindowHook:invalid_hWnd");
		return FALSE;
	}
}

boolean UninstallHook() {
	boolean result = FALSE;
	result = UnhookWindowsHookEx(hCallWndHook);
	result = UnhookWindowsHookEx(hMouseHook);
	if (!result) {
		printErr(L"UninstallHook:UnhookWindowsHookEx");
	}
	return result;

}

HANDLE hProcess;

int main(int argc, char** argv) {
	//Проверяем, запущен ли Skype (в соновном нужно для разработки)
	HWND hwMain = FindWindowW(WNC_MAIN_FORM, NULL);

	//Ждём главного окна Skype, если ранее его не нашли
	while (!hwMain) {
		hwMain = FindWindowW(WNC_MAIN_FORM, NULL);
		Sleep(1000);
	}
	DWORD processId = 0;
	DWORD threadId = GetWindowThreadProcessId(hwMain, &processId);
	logArg(L"Skype thread %ld .InstallWindowHook... \n", threadId);
	if (!hProcess) { //если не знаем указатель на процесс, то открываем его
		hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, processId);
		if (!hProcess) {
			printErr(L"OpenProcess");
			return -1;
		}
	}
	//Ставим хук
	if (InstallWindowHook(hwMain)) {
		log(L"Wait Skype process...");
		WaitForSingleObject(hProcess, INFINITE);
		log(L"UninstallHook...");
		if (!UninstallHook()) {
			printErr(L"InstallWindowHook");
			return -1;
		}
	} else {
		printErr(L"InstallWindowHook");
		return -1;
	}

	return 0;
}

