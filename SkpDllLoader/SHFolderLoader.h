#define ADDCALL __cdecl

#ifndef SHFOLDERLOADER_
#define SHFOLDERLOADER_

#include <windows.h>
//#include <shfolder.h>


__declspec(dllexport)
#define EXPORT_DLL __declspec(dllexport)
EXPORT_DLL BOOL APIENTRY DllMain(PVOID hModule, ULONG ulReason, PCONTEXT pctx);
EXPORT_DLL HRESULT WINAPI SHGetFolderPathA(HWND hwnd, int csidl, HANDLE hToken, DWORD dwFlags, LPSTR pszPath);
EXPORT_DLL HRESULT WINAPI SHGetFolderPathW(HWND hwnd, int csidl, HANDLE hToken, DWORD dwFlags, LPWSTR pszPath);

#endif /* SHFOLDERLOADER_ */

