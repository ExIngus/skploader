#include "SHFolderLoader.h"
#include "../SkpHook/SkpHook.h"
#include <windows.h>
#include <wchar.h>
//#include <shfolder.h>

typedef HRESULT(__stdcall *PFNSHGETFOLDERPATHA)(HWND, int, HANDLE, DWORD, LPSTR);
typedef HRESULT(__stdcall *PFNSHGETFOLDERPATHW)(HWND, int, HANDLE, DWORD, LPWSTR);

PFNSHGETFOLDERPATHA pGetFolderPathA;
PFNSHGETFOLDERPATHW pGetFolderPathW;

void printErr(LPCWSTR mark) {
	//https://msdn.microsoft.com/en-us/library/windows/desktop/ms681381%28v=vs.85%29.aspx
	DWORD err = GetLastError();
	LPWSTR pBuffer;
	FormatMessageW(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM, NULL, err,
			MAKELANGID(LANG_ENGLISH, SUBLANG_DEFAULT), (LPWSTR) & pBuffer, 0, NULL);
	WCHAR strBuff[MAX_STACK_STR];
	wsprintfW(strBuff, L"Error:%s: %ld : %s", mark, err, pBuffer);
	OutputDebugStringW(strBuff);
}

HHOOK hCallWndHook;
HHOOK hMouseHook;

boolean InstallHook(DWORD dwThreadID) {
	if (dwThreadID) {
		HMODULE mHook = LoadLibraryW(HLIB_NAME);
		if (mHook) {
			FARPROC pCallWndProc = GetProcAddress(mHook, HLIB_CALL_WHN_PROC);
			FARPROC pMouseProc = GetProcAddress(mHook, HLIB_MOUSE_PROC);
			if (pCallWndProc && pMouseProc) {
				hCallWndHook = SetWindowsHookExW(WH_CALLWNDPROC, (HOOKPROC) pCallWndProc, mHook, dwThreadID);
				hMouseHook = SetWindowsHookExW(WH_MOUSE, (HOOKPROC) pMouseProc, mHook, dwThreadID);
				if (hCallWndHook && hMouseHook) {
					return TRUE;
				} else {
					printErr(L"InstallHook:SetWindowsHookExW");
					return FALSE;
				}
			} else {
				printErr(L"InstallHook:GetProcAddress");
				return FALSE;
			}
		} else {
			printErr(L"InstallHook:LoadLibraryW");
			return FALSE;
		}

	} else {
		printErr(L"InstallHook:invalid_dwThreadID");
		return FALSE;
	}
}

boolean InstallWindowHook(HWND hWnd) {
	if ((hWnd) && (IsWindow(hWnd))) {
		DWORD dwThreadID = GetWindowThreadProcessId(hWnd, NULL);
		return InstallHook(dwThreadID);
	} else {
		printErr(L"InstallWindowHook:invalid_hWnd");
		return FALSE;
	}
}

int timerId;

VOID CALLBACK HookTimerProc(HWND hwnd, UINT uMsg, UINT_PTR idEvent, DWORD dwTime) {
	log(L"HookTimerProc");
	HWND hwMain = FindWindowW(WNC_MAIN_FORM, NULL);
	if (hwMain) {
		KillTimer(NULL, timerId);
		if (!InstallWindowHook(hwMain)) {
			printErr(L"InstallWindowHook");
		}

	}
}

BOOL APIENTRY DllMain(PVOID hModule, ULONG ulReason, PCONTEXT pctx) {
	UNREFERENCED_PARAMETER(hModule);
	UNREFERENCED_PARAMETER(pctx);

	if (ulReason == DLL_PROCESS_ATTACH) {
		WCHAR patch[MAX_PATH];
		GetModuleFileNameW(NULL, patch, MAX_PATH);
		logArg(L"SHFolder loaded to %s", patch);
		if (wcsstr(patch, L"Skype.exe")) {
			//далее инициализация только для Skype.exe
			WCHAR systemDir[MAX_PATH + 20]; //MAX_PATH + L"\\shfolder.dll"
			if(!GetSystemWow64DirectoryW(systemDir, MAX_PATH)){
				//ошибка GetSystemWow64DirectoryW, система 32-разрядная
				log(L"GetSystemWow64Directory error, use GetSystemDirectory");
				GetSystemDirectoryW(systemDir, MAX_PATH);
			}
			logArg(L"SystemDirectory: %s", systemDir);
			wcscat(systemDir, L"\\shfolder.dll");
			logArg(L"LoadLibrary %s", systemDir);
			HMODULE hSHFolder = LoadLibraryW(systemDir);
			if (hSHFolder) {
				pGetFolderPathA = (PFNSHGETFOLDERPATHA) GetProcAddress(hSHFolder, "SHGetFolderPathA");
				pGetFolderPathW = (PFNSHGETFOLDERPATHW) GetProcAddress(hSHFolder, "SHGetFolderPathW");
				if (pGetFolderPathA && pGetFolderPathW) {
					logArg(L"Loaded %s", systemDir);
					timerId = SetTimer(NULL, 0, 1000, (TIMERPROC) HookTimerProc);
					logArg(L"SetTimer %d", timerId);
					if (!timerId) {
						printErr(L"SetTimer");
					}
					return TRUE;
				}
			}
			return FALSE;
		}
		logArg(L"SHFolder not loaded to %s", patch);
		return FALSE;
	}

	return TRUE;
}

HRESULT WINAPI SHGetFolderPathA(HWND hwnd, int csidl, HANDLE hToken, DWORD dwFlags, LPSTR pszPath) {
	log(L"SHGetFolderPathA");
	return pGetFolderPathA(hwnd, csidl, hToken, dwFlags, pszPath);
}

HRESULT WINAPI SHGetFolderPathW(HWND hwnd, int csidl, HANDLE hToken, DWORD dwFlags, LPWSTR pszPath) {
	log(L"SHGetFolderPathW");
	return pGetFolderPathW(hwnd, csidl, hToken, dwFlags, pszPath);

}


